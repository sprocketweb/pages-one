<?php namespace Sprocket\Pages;

use Illuminate\Support\ServiceProvider;

class PagesServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('sprocket/pages');

		// AliasLoader::getInstance()->alias('Pages', 'Sprocket\Pages\Helpers\Pages');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		include __DIR__.'/../../handlers.php';
		include __DIR__.'/../../routes.php';

		// $this->app->bind('Sprocket\Cms\Repo\Repository','Sprocket\Pages\Repo\PagesRepo');

	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
