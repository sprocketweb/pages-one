<?php namespace Sprocket\Pages;

use \Config;
use \Event;
use \File;
use \Input;
use \Redirect;
use \Response;
use \Request;
use \Str;
use \View;
use Sprocket\Cms\BaseController;
// use Sprocket\Pages\Services\Validation\PageValidator as Validator;

class UploadController extends BaseController {

	public function __construct()
	{

		// $this->beforeFilter('isSuper', array('except' => ['index','edit','update','show']) );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

	}

	public function image()
	{
		$file = Input::file('file');

		$rules = Config::get('pages::image.rules','max:1536|image|mimes:jpg,jpeg,gif,png');
		$validation = \Validator::make(Input::all(),['file'=>$rules]);

		if ($validation->fails())
		{
			$msgs = $validation->messages();
			$msg = $msgs->first('file');
			return stripslashes(json_encode(["error" => "{$msg}"]));
			exit();
		}

		$dir = $_POST['upload_dir'];
		$default_dir = Config::get('pages::image.dir.uploads','uploads');
		$dir = ( ! empty($dir)) ? $dir : $default_dir;

		$_FILES['file']['type'] = strtolower($_FILES['file']['type']);

		$asset_dir = '/assets/images';
		// $asset_dir = Config::get('pages::image.dir.img','/assets/images/');
		$dest_dir = $asset_dir .'/'. Str::slug($dir);

		if ( ! File::isDirectory(public_path() . $dest_dir )) File::makeDirectory(public_path() . $dest_dir);

		$pathinfo = pathinfo($_FILES['file']['name']);
		$name = Str::slug($pathinfo['filename'])
			. '.'
			. strtolower($pathinfo['extension']);

		$new_file_loc = public_path()."/{$dest_dir}/{$name}";

		File::move( $_FILES['file']['tmp_name'] , $new_file_loc);

		$link = ['filelink' => $dest_dir.'/'.$name];

		return stripslashes(json_encode($link));
	}



	public function listing()
	{
		// 	{"thumb": "/tests/_images/1_m.jpg", "image": "/tests/_images/1.jpg", "title": "Image 1", "folder": "Folder 1" },
		$images = [];
		$img_dir = public_path() . '/assets/images';

		$dirs = File::directories($img_dir);

		foreach ($dirs as $dir) {

			$files = File::files($dir);

			foreach ($files as $file) {

				$path_parts = pathinfo($file);

				// $directory_array = explode('/', $path_parts['dirname']);
				$parent = last(explode('/', $path_parts['dirname']));
				// $parent = last($directory_array);

				$images[] = [
					'image'		=> '/assets/images/'.$parent.'/'.$path_parts['basename'],
					'thumb'		=> '/assets/images/'.$parent.'/'.$path_parts['basename'],
					'folder'	=> $parent
				];

			}

		}

		return Response::json($images);
		// return stripslashes(json_encode($files));
	}

	/**
	 * image uploader
	 * @return array of image information
	 */
	public function post_imageupload(){
		$validation = Validator::make(Input::all(),
			['file'=> 'max:10240', 'file'=> 'image', 'file'=> 'mimes:jpg,gif,png'],
			['file_image'=> 'Your file must be an image.', 'file_max'=> 'Your image was too large. Please make it smaller.', 'file_mimes'=> 'This does not appear to be an image file.']
			);
		$file = Input::file('file');

		if ($validation->fails())
		{
			$msg = $validation->errors->first('file');
			return stripslashes(json_encode(["error" => "There was a problem with your image. {$msg}"]));
			exit();
		}

		$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
		$filename = pathinfo($file['name'], PATHINFO_FILENAME);
		$clean_filename = strtolower( Str::slug($filename, '-') . ".{$ext}" );

		if (Input::upload('file', Config::get('admin::editor.uploads.dir','public/uploads'),
			$clean_filename ) )
		{
			$array = array('filelink' =>
				Config::get('admin::editor.uploads.path','/uploads/') . $clean_filename
		);
			return stripslashes(json_encode($array));
		}

	}


}
