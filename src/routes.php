<?php

Route::group(['prefix'=>'admin','before'=>'auth'], function()
{
	Route::resource('pages','Sprocket\Pages\PageController');
	// Route::post('/upload/image/{dir?}', ['uses'=>'Sprocket\Pages\UploadController@image', 'as'=>'admin.image.upload']);
	Route::post('/upload/image', ['uses'=>'Sprocket\Pages\UploadController@image', 'as'=>'admin.image.upload']);

	Route::get('/image/listing', ['uses'=>'Sprocket\Pages\UploadController@listing', 'as'=>'admin.image.listing']);
});
