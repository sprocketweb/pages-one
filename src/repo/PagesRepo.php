<?php namespace Sprocket\Pages\Repo;

use Sprocket\Pages\Repo\Pages as Page;
use Sprocket\Pages\Repo\PagesRepositoryInterface;
use Sprocket\Cms\Repo\DbRepository;

class PagesRepo extends DbRepository implements PagesRepositoryInterface {

	protected $model;

	public function __construct(Page $model)
	{
		$this->model = $model;
	}

	public function getAll()
	{
		return $this->model->orderBy('order')->get();
	}

}
