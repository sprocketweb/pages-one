<?php namespace Sprocket\Pages\Repo;

use \Eloquent;

class Pages extends Eloquent {

	protected $guarded = array();

	protected $softDelete = true;

	/* 'title','content','slug','menu','description','section','published','searchable','autosave','cacheable', */

}
