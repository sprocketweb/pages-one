<?php namespace Sprocket\Pages\Repo;

interface PagesRepositoryInterface {

	public function getAll();

	public function getById($id);

	public function delete($id);

}
