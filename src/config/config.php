<?php

return array(

	'version'		=> '0.1.0 alpha',
	'has_sections'	=> false,
	'has_media'		=> false,
	'has_blog'		=> false,

	'image'			=>
	[
		'rules'		=> 'max:1536|image|mimes:jpg,jpeg,gif,png',

		'dir'		=> [
			'img' 		=> 'assets/images',	// image directory
			'uploads' 	=> 'uploads',			// default upload directory within asset images directory
		]

	],

);
