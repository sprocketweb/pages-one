@extends('pages::layouts.main')

@section('content')

@if(Auth::user()->super)

<p>
	<a href="{{ action('Sprocket\Pages\PageController@create')}}" class="btn btn-lg btn-info pull-right">
	<i class="fa fa-plus"></i>
	Add a new page</a>
</p>

@endif

<h1>All Pages</h1>

@if ($pages->count())

<table class="table table-bordered table-hover table-striped">
<tbody>
<thead>
	<th>Title</th>
	<th>Last Updated</th>
	<th>&nbsp;</th>
</thead>
@foreach($pages as $page)
<tr>
	<td>
		<strong>{{ $page->title }}</strong>
		<br>

		<a href="{{ $_SERVER['SERVER_NAME'] }}/{{ $page->slug }}">
			<i class="fa fa-link"></i>
			{{ $page->slug }}
		</a>

	</td>
	<td>
		{{ Cms::daysAgo($page->updated_at) }}
	</td>
	<td>
	@if(Auth::user()->super)
	{{ Form::open( [
		'url'=> URL::route('admin.pages.destroy',[$page->id]),
		'method' => 'delete',
		'class' => 'form-inline',
		'role' => 'form'
		]) }}
	<button class="btn btn-danger pull-right pages-btn-delete"><i class="fa fa-exclamation-triangle"></i> Delete</button>
	{{ Form::close() }}
	@endif
	<a href="{{ action('Sprocket\Pages\PageController@edit',$page->id) }}" class="btn btn-info btn-small"><i class="fa fa-pencil"></i> Edit</a>

	<a href="{{ action('Sprocket\Pages\PageController@show',$page->id) }}" class="btn btn-info btn-small pages-btn-preview"><i class="fa fa-eye"></i> Preview</a>

	</td>
</tr>
@endforeach
</tbody>
</table>

@include('pages::preview_modal')
{{ Cms::totals('page', $pages->count(), 'footer-details') }}

@else
	<p class="lead">There are no pages.</p>
@endif

@stop
