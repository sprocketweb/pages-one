@extends('pages::layouts.main')

@section('content')
<h1>Create a new page</h1>

@include('pages::errors')

<div class="row col-md-10 col-md-offset-1">


{{ Form::open( [
	'url'=> URL::route('admin.pages.index'),
	'method' => 'post'
	]) }}

@include('pages::page_settings')

{{ Form::textarea('content',null,['class'=>'', 'id'=>'redactor']) }}

@include('cms::layouts.forms.basicactions')

{{ Form::close() }}

@stop
