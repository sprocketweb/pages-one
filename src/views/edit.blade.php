@extends('pages::layouts.main')

@section('content')
<h1>Edit: {{ $page->title }} </h1>

@include('pages::errors')

<div class="row col-md-10 col-md-offset-1">

{{ Form::model( $page, ['url'=> action('Sprocket\Pages\PageController@update', $page->id), 'method' => 'put', 'id' => 'pages-form-edit']) }}
{{ Form::hidden('user_id',Auth::user()->id) }}

<div class="btn-toolbar pages-edit-shortcut-btn-toolbar pull-right" role="toolbar">
<div class="btn-group btn-group-sm pull-right">
	<a href="#" data-toggle="tooltip" class="btn btn-default btn-sm btn-hover-success" id="pages-btn-shortcut-quicksave" title="Quick Save" accesskey=""><i class="fa fa-clock-o"></i></a>
	<a href="#" data-toggle="tooltip" class="btn btn-default btn-sm btn-hover-success" id="pages-btn-shortcut-save" title="Save"><i class="fa fa-save"></i></a>
</div>

<div class="btn-group btn-group-sm pull-right">
	<a href="{{ Request::url() }}" data-toggle="tooltip" class="btn btn-default btn-sm btn-hover-warning" title="Reload"><i class="fa fa-repeat"></i></a>
	<a href="{{ route('admin.pages.index') }}" data-toggle="tooltip" class="btn btn-default btn-sm btn-hover-warning" title="Cancel"><i class="fa fa-minus-circle"></i></a>
</div>
</div>
<ul class="nav nav-tabs" id="editor_tabs">
	<li class="active"><a href="#page_content" data-target="#page_content"><i class="fa fa-file-text"></i> Content</a></li>
	<li><a href="#page-settings" data-target="#page-settings"><i class="fa fa-wrench"></i> Settings</a></li>
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="page_content">
	{{ Form::textarea('content',null,['id'=>'redactor']) }}
	</div><!-- page_content -->

	<div class="tab-pane" id="page-settings">
	@include('pages::page_settings')
	</div><!-- page-settings -->

@include('cms::layouts.forms.basicactions')

</div><!-- tab-content -->

{{ Form::close() }}
</div><!-- wrap -->

<div class="footer-details" id="redactor-autosave">&nbsp;</div>

@stop
