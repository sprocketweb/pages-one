<?php namespace Sprocket\Pages\Services\Validation;

use Sprocket\Cms\Services\Validation\Validator;

class PageValidator extends Validator {

	static $rules = [
			'title' => 'required',
			'content' => 'required',
			'slug' => 'sometimes|alpha_dash',
			'menu' => 'sometimes',
			'description' => 'required',
			'published' => 'in:0,1',
			'searchable' => 'in:0,1',
			'cacheable' => 'in:0,1',
			// 'autosave' => 'in:0,1',
			// 'user_id' => 'required|integer',
	];

}

